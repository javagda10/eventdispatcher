package com.sda.android;

import com.sda.android.events.CallEndedEvent;
import com.sda.android.events.CallStartedEvent;
import com.sda.android.models.AndroidSystem;
import com.sda.restauracja.EventDispatcher;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        AndroidSystem system = new AndroidSystem();

        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            String command = line.split(" ")[0];
            int call_id = Integer.parseInt(line.split(" ")[1]);
            if (command.equalsIgnoreCase("start")) {
                system.rozpocznijPolaczenie(call_id);
            } else if (command.equalsIgnoreCase("stop")) {
                system.zakonczPolaczenie(call_id);
            }
        }
    }

}
