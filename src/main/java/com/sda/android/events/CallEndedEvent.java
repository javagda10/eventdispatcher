package com.sda.android.events;

import com.sda.android.interfejsy.ICallListener;
import com.sda.restauracja.EventDispatcher;
import com.sda.restauracja.zdarzenia.IEvent;

import java.util.List;

public class CallEndedEvent implements IEvent {

    private int callId;

    public CallEndedEvent(int callId) {
        this.callId = callId;
    }

    @Override
    public void execute() {
        List<ICallListener> list =
                EventDispatcher.getInstance().pobierzObiektyImplementujaceInterfejs(ICallListener.class);
        for (ICallListener listener : list) {
            listener.callEnded(callId);
        }
    }
}
