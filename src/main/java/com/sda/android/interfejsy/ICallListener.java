package com.sda.android.interfejsy;

public interface ICallListener {
    void callStarted(int callId);

    void callEnded(int callId);
}
