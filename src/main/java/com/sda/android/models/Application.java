package com.sda.android.models;

import com.sda.restauracja.EventDispatcher;

public abstract class Application {
    public Application() {
        EventDispatcher.getInstance().register(this);
    }
}
