package com.sda.android.models;

import com.sda.android.interfejsy.ICallListener;

public class CallRecorderApplication extends Application implements ICallListener {
    @Override
    public void callStarted(int callId) {
        System.out.println("CallRecorderApplication - > started -> " + callId);
    }

    @Override
    public void callEnded(int callId) {
        System.out.println("CallRecorderApplication - > ended -> " + callId);
    }
}
