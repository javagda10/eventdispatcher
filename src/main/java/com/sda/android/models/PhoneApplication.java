package com.sda.android.models;

import com.sda.android.interfejsy.ICallListener;

public class PhoneApplication extends Application implements ICallListener {
    @Override
    public void callStarted(int callId) {
        System.out.println("PhoneApplication - > started -> " + callId);
    }

    @Override
    public void callEnded(int callId) {
        System.out.println("PhoneApplication - > ended -> " + callId);
    }
}
