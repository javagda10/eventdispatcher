package com.sda.restauracja.modele;


import com.sda.restauracja.EventDispatcher;

public abstract class AbstractRegistrable {
    public AbstractRegistrable() {
        EventDispatcher.getInstance().register(this);
    }
}
