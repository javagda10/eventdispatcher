package com.sda.restauracja.zdarzenia;

public interface IEvent{
    void execute();
}
