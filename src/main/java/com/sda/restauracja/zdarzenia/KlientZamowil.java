package com.sda.restauracja.zdarzenia;

import com.sda.restauracja.EventDispatcher;
import com.sda.restauracja.interfejsy.IPowiadamialnyOZamowieniu;

import java.util.ArrayList;
import java.util.List;

public class KlientZamowil implements IEvent{
    private String imie;

    public KlientZamowil(String imie) {
        this.imie = imie;
    }

    @Override
    public void execute() {
        List<IPowiadamialnyOZamowieniu> powiadamialne = EventDispatcher.getInstance().pobierzObiektyImplementujaceInterfejs(IPowiadamialnyOZamowieniu.class);
        for (IPowiadamialnyOZamowieniu powiadamialny : powiadamialne) {
            powiadamialny.zlozylZamowienie(imie, new ArrayList<String>());
        }
    }
}
