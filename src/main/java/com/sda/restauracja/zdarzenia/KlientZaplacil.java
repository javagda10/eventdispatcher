package com.sda.restauracja.zdarzenia;

import com.sda.restauracja.EventDispatcher;
import com.sda.restauracja.interfejsy.IPowiadamialnyOZaplacieZaPosilek;

import java.util.List;

public class KlientZaplacil implements IEvent {
    private String imieKlienta;

    public KlientZaplacil(String imieKlienta) {
        this.imieKlienta = imieKlienta;
    }

    @Override
    public void execute() {
        List<IPowiadamialnyOZaplacieZaPosilek> powiadamialne = EventDispatcher.getInstance().pobierzObiektyImplementujaceInterfejs(IPowiadamialnyOZaplacieZaPosilek.class);
        for (IPowiadamialnyOZaplacieZaPosilek powiadamialny : powiadamialne) {
            powiadamialny.klientZaplacil(imieKlienta);
        }
    }
}
